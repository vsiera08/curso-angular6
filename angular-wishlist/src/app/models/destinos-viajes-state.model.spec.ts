import { reducerDestinosViajes, DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction } from "./destinos-viajes-state.model";
import { DestinoViaje } from "./destino-viaje.model";

describe('reducerDestinosViajes', () => {
  it('should reduce init data', () => {
    // Setup
    const prevState: DestinosViajesState =initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1','destino 2']);
    // Action
    const newState: DestinosViajesState = reducerDestinosViajes(private, action);
    // Assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });
  it('should reduce init data', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction('barcelona','url');
    const newState: DestinosViajesState = reducerDestinosViajes(private, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });
});
