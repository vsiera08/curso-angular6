import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-whishlist';
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
  });
  destinoAgregado(d) {
  }
  constructor(private translateService: TranslateService) {
    console.log('***********GetTranslation');
    translateService.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    translateService.setDefaultLang('es');
  }
}
