Curso de Angular 6

explorer
  move componentes a  components
cmd line
  ng g guard guards/usuario-logueando/usuario-logueado
  ng g s services/auth
  ng g c components/login/login
  ng g c components/protected/protected

angular-whishlist/src/app/components/login/login.component.htmh
<form class="form-inline" *ngIf="!authService.getUser()">
  <div class="form-group">
    <label for="username">User: (type<em>user</em>)</label>
    <input class="form-control" name="username" #username>
  </div>
  <div class="form-group">
    <label for="password">Password: (type<em>password</em>)</label>
    <input class="form-control" type=""password name="password" #password>
  </div>
  <a class="btn btn-default" (click)="login(usename.value, password.value)">Entrar!</a>
  {{mensajeError}}
</form>
<a class="btn btn-default" (click)="logout()" *ngIf="authService.getUser()">Salir!</a>

angular-whishlist/src/app/components/login/login.component.htmh
  export class LoginComponent implements OnInit{
    mensajeError: String;

    constructor(public authService: AuthService){
      this.mensajeError = '';
    }
    ngOnInit() {
  }
  login(username: String, password: String):boolean{
    this.mensajeError = '';
    if (!this.authService.login(username, password)) {
      this.mensajeError = 'Login Incorrecto.';
      setTimeout(function) {
        this.mensajeError = '';
      }.bind(this),2500
    }
    return false;
  }
  logout(): boolean{
    this.authService.logout();
    return false;
  }
  }

<!-- ----------------------------------------------------------------------- -->
cmd line

  ng g c components/vuelos/vuelos-component
  ng g c components/vuelos/vuelos-main-component
  ng g c components/vuelos/vuelos-mas-info-component
  ng g c components/vuelos/vuelos-detalle-component

  angular-whishlist/src/app/components/vuelos/vuelos/vuelos-component.html

  <p>
    Vuelos Works!
  </p>
  <div class="navLinks">
    <a [routerLink]="['./main']">Main</a>  |
    <a [routerLink]="['./mas-info']">Mas Info</a> |
    <a [routerLink]="['./',5]">Vuelo 5</a>
  </div>
  <router-outlet></router-outlet>

  angular-whishlist/src/app/components/vuelos/vuelos-detalle/vuelos-detalle-component.html

  <p>
    vuelos-detalle works! => id {{id}}
  </p>

angular-whishlist/src/app/components/vuelos/vuelos-detalle/vuelos-detalle-component.ts
export class VuelosDetalleComponent implements OnInit {
  id: any;

  constructor ( private route: ActivatedRoute ) {
    route.params.subscribe ( params => { this.id = params['id'];});
  }
  ngOnInit () {
  }
}
<!-- ------------------------------------------------------------------------------------------------ -->

ng g m reservas/reservas --module app --flat --routing
ng g c reservas/reservas-listado
ng g c reservas/reservas-detalle
